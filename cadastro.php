<?php

/*
Plugin Name: WP SNA - Abaixo-assinado
Description: Plugin Abaixo-assinado desenvolvido por Marcelo Campos
Version: 0.1
Author: Marcelo Rodrigues
Author URI: https://github.com/wwwxkz
*/

define('ABAIXO_ASSINADO_LOCAL', plugin_dir_path(__FILE__));
require_once(ABAIXO_ASSINADO_LOCAL . 'funcionalidades/cadastros/cadastros.php');
require_once(ABAIXO_ASSINADO_LOCAL . 'funcionalidades/cadastrar/cadastrar.php');

add_action('admin_menu', 'abaixo_assinado');
function abaixo_assinado(){
	add_menu_page( 'Abaixo-assinado', 'Abaixo-assinado', 'manage_options', 'abaixo_assinado', 'abaixo_assinado', 'dashicons-welcome-widgets-menus', 10 );
	add_submenu_page( 'abaixo_assinado', 'Listagem', 'Listagem', 'read', 'abaixo_assinado_cadastros', 'abaixo_assinado_cadastros' );
	add_submenu_page( 'abaixo_assinado', 'Novo Cadastro', 'Novo Cadastro', 'read', 'abaixo_assinado_cadastrar', 'abaixo_assinado_cadastrar' );
	remove_submenu_page('abaixo_assinado','abaixo_assinado');
}

register_activation_hook(__FILE__, 'abaixo_assinado_ativacao');
function abaixo_assinado_ativacao(){
	global $wpdb;
	$tabela = "sna_abaixo_assinado";
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $tabela (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		criado timestamp NOT NULL default CURRENT_TIMESTAMP,
		nome tinytext NOT NULL,
		estado varchar(255) NOT NULL,
		municipio varchar(255) NOT NULL,
		anac varchar(255) NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
}

register_uninstall_hook(__FILE__, 'abaixo_assinado_desinstalacao');
function abaixo_assinado_desinstalacao(){
	$tabela = "sna_abaixo_assinado";
	$sql = "DROP TABLE IF EXISTS $tabela";
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
}
?>
