<?php
function abaixo_assinado_cadastrar_formulario()
{
    if (isset($_POST['abaixo-assinado-cadastrar']) || isset($_POST['abaixo-assinado-editar'])) {
        global $wpdb;
        $nome = $_POST['abaixo-assinado-nome'];
        $anac = $_POST['abaixo-assinado-anac'];
        $nome_completo = explode(' ', $nome);

        if (sizeof($nome_completo) >= 2) {
            $usuarios = $wpdb->get_results("SELECT 1 FROM sna_abaixo_assinado WHERE anac = $anac;");
            if (isset($usuarios[0]) && $usuarios[0] >= 1 && isset($_POST['cadastrar'])) {
                echo "<script>alert('ANAC já cadastrado')</script>";
                return 1;
            } else {
                if (isset($_POST['abaixo-assinado-id'])) {
                    $id = $_POST['id'];
                    if (isset($_POST['abaixo-assinado-estado']) && isset($_POST['abaixo-assinado-municipio'])){
                        $wpdb->replace(
                            'sna_abaixo_assinado',
                            array(
                                'id' => $id,
                                'nome' => $nome,
                                'estado' => $_POST['abaixo-assinado-estado'],
                                'municipio' => $_POST['abaixo-assinado-municipio'],
                                'anac' => $anac,
                            )
                        );
                    }
                    elseif (isset($_POST['abaixo-assinado-estado'])){
                        $wpdb->replace(
                            'sna_abaixo_assinado',
                            array(
                                'id' => $id,
                                'nome' => $nome,
                                'estado' => $_POST['abaixo-assinado-estado'],
                                'municipio' => '',
                                'anac' => $anac,
                            )
                        );
                    }
                    elseif (isset($_POST['abaixo-assinado-municipio'])){
                        $wpdb->replace(
                            'sna_abaixo_assinado',
                            array(
                                'id' => $id,
                                'nome' => $nome,
                                'estado' => '',
                                'municipio' => $_POST['abaixo-assinado-municipio'],
                                'anac' => $anac,
                            )
                        );
                    }
                    else {
                        $wpdb->replace(
                            'sna_abaixo_assinado',
                            array(
                                'id' => $id,
                                'nome' => $nome,
                                'estado' => '',
                                'municipio' => '',
                                'anac' => $anac,
                            )
                        );
                    }
                    return 0;
                } else {
                    if (isset($_POST['abaixo-assinado-aceite'])) {
                        if (isset($_POST['abaixo-assinado-estado']) && isset($_POST['abaixo-assinado-municipio'])){
                            $wpdb->replace(
                                'sna_abaixo_assinado',
                                array(
                                    'nome' => $nome,
                                    'estado' => $_POST['abaixo-assinado-estado'],
                                    'municipio' => $_POST['abaixo-assinado-municipio'],
                                    'anac' => $anac,
                                )
                            );
                        }
                        elseif (isset($_POST['abaixo-assinado-estado'])){
                            $wpdb->replace(
                                'sna_abaixo_assinado',
                                array(
                                    'nome' => $nome,
                                    'estado' => $_POST['abaixo-assinado-estado'],
                                    'municipio' => '',
                                    'anac' => $anac,
                                )
                            );
                        }
                        elseif (isset($_POST['abaixo-assinado-municipio'])){
                            $wpdb->replace(
                                'sna_abaixo_assinado',
                                array(
                                    'nome' => $nome,
                                    'estado' => '',
                                    'municipio' => $_POST['abaixo-assinado-municipio'],
                                    'anac' => $anac,
                                )
                            );
                        }
                        else {
                            $wpdb->replace(
                                'sna_abaixo_assinado',
                                array(
                                    'nome' => $nome,
                                    'estado' => '',
                                    'municipio' => '',
                                    'anac' => $anac,
                                )
                            );
                        }
                        return 0;
                    } else {
                        echo "<script>alert('Por favor leia e aceite o Termo de uso antes de cadastrar')</script>";
                    }
                }
            }
        } else {
            echo "<script>alert('Por favor inserir nome completo')</script>";
            return 1;
        }
    }
}
