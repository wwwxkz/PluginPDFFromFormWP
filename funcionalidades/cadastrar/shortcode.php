<?php

function abaixo_assinado_cadastrar_shortcode()
{
    global $wpdb;
    $assinaturas = $wpdb->get_results("SELECT COUNT(*) FROM `sna_abaixo_assinado`;");
    $retorno = '
    <form method="post" style="display: flex; justify-content: center;">
        <div style="display: flex; flex-direction: column; max-width: 825px;">
		<object data="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastrar\assets\images\peticao-publica.pdf" type="application/pdf" width="100%" height="565px">
			<embed src="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastrar\assets\images\peticao-publica.jpg" width="100%" height="565px" style="object-fit: contain;"/>
		</object>
      	    <br>
            <h5 style="width: 100%; text-align: center;"><b>' . number_format(reset($assinaturas[0]), 0, '', '.') . '</b> assinaturas até o momento das <b>10.000</b> inicialmente objetivadas</h5>
            <div style="width: 100%; height: 30px; border: solid 2px black;">
              <div style="display: flex; background-color: #1e73be; width: ' . round(reset($assinaturas[0])/10000 * 100) . '%; height: 100%; align-items: center; justify-content: center;">
                <h5 style="color: white; margin-bottom: 0;">' . round(reset($assinaturas[0])/10000 * 100) . '%</h5>
              </div>
            </div>
            <br>
            <label>Nome&nbsp<a style="color: red;">*</a></label>
            <input type="text" oninput="validar_nome()" id="nome" required name="abaixo-assinado-nome" placeholder="Nome completo" />
            <label>ANAC&nbsp<a style="color: red;">*</a></label>
            <input type="text" required name="abaixo-assinado-anac" placeholder="Código ANAC " />
            <label>Estado (opcional)</label>
            <select id="estado" name="abaixo-assinado-estado">
                <option value="">Selecione seu Estado</option>
            </select>
            <label>Município (opcional)</label>
            <select id="municipio" name="abaixo-assinado-municipio">
                <option value="">Selecione seu Município</option>
            </select>
            <table class="wp-list-table widefat striped projeto">
                <thead>
                    <tr>
                        <th class="table-header">
                            <a>Termo de concordância</a>
                        </th>
                    </tr>
                </thead>
                <tbody id="projeto">
                    <tr>
                        <th>
                            <h6>
								Duis eu vehicula nibh. Fusce imperdiet, ante quis maximus interdum, tortor urna aliquet massa, nec lacinia tortor augue nec urna. Phasellus consequat vitae ex vitae consequat. Etiam vel nunc enim. Maecenas eu interdum dolor. Proin at euismod augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id blandit libero, sit amet vehicula leo. Quisque tincidunt magna ligula, eu suscipit diam pellentesque a.
                            </h6>
                        </th>
                    </tr>
                </tbody>
            </table>
            <div style="margin-top: 15px;">
                <input type="checkbox" required id="aceite" name="abaixo-assinado-aceite">
                <label for="aceite">&nbsp ACEITO</label>
            </div>
            <a style="margin-top: 10px; margin-bottom: 5px;" href="">Política de privacidade</a>
            <input type="submit" required class="cadastrar" name="abaixo-assinado-cadastrar" value="Assinar" />
        </div>
        <br>
    </form>
    <style>
        table {
            margin-top: 20px !important;
        }

        input::-webkit-calendar-picker-indicator {
            opacity: 0;
        }

        .cadastrar {
            margin-top: 10px !important;
            font-size: 100% !important;
            padding: 14px !important;
            color: white !important;
            background: #1e73be !important;
            border: none !important;
            border-radius: 0 !important;
        }

        .table-header {
            display: flex;
            justify-content: space-between;
            padding: 10px !important;
        }

        .modal {
            position: fixed;
            align-content: center;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
        }

        .conteudo-modal {
            position: fixed;
            top: 20%;
            left: 50%;
            background-color: #fefefe;
            padding: 30px;
        }

        .fechar {
            color: #aaaaaa;
            position: absolute;
            top: 10px;
            right: 10px;
            font-size: 28px;
            font-weight: bold;
        }

        .fechar:hover,
        .fechar:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .projeto {
            margin-top: 10px !important;
        }

        #projeto > tr > th > h6 {
            padding: 10px;
      			color: #64686d;
      			text-align: left;
            text-align: justify;
            text-justify: inter-word;
		}

		th {
			border: none !important;
		}

        a {
            text-decoration: none !important;
        }
    </style>
    <script>
        var script = document.createElement("script");
        script.src = "https://code.jquery.com/jquery-3.6.0.min.js";
        document.getElementsByTagName("head")[0].appendChild(script);

        function validar_nome() {
            nome = document.getElementById("nome");
            nome.setAttribute("style", "color: red !important")
            if (nome.value.split(" ").length >= 2){
                nome.setAttribute("style", "color: green !important")
            }
        }

        var jquery = function (callback) {
            if (window.jQuery) {
                callback(jQuery);
            }
            else {
                window.setTimeout(function () { jquery(callback); }, 20);
            }
        };

        jquery(function ($) {
            $(function () {
                $.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/", function (data) {
                    data.forEach(estado => {
                        $("#estado").append("<option value=" + estado.sigla + ">" + estado.nome + "</option>");
                    });
                });

                $("#estado").change(function (e) {
                    $("#municipio").empty();
                    var options =
                        $.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/" + $("#estado option:selected").val() + "/municipios/", function (data) {
                            data.forEach(municipio => {
                                $("#municipio").append("<option value=\'" + municipio.nome + "\'>" + municipio.nome + "</option>");
                            });
                        });
                });
            });
        });


    </script>
	';

    return $retorno;
}
