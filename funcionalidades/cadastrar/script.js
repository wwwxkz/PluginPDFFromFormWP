var script = document.createElement('script');
script.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
document.getElementsByTagName('head')[0].appendChild(script);

function validar_nome() {
    nome = document.getElementById("nome");
    nome.style.color = 'red';
    if (nome.value.split(" ").length >= 2) {
        nome.style.color = 'green';
    }
}

var jquery = function (callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function () { jquery(callback); }, 20);
    }
};

jquery(function ($) {
    $(function () {
        $.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/", function (data) {
            data.forEach(estado => {
                $('#estado').append(
                    `<option value="${estado.sigla}">
                        ${estado.nome}
                    </option>`
                );
            });
        });

        $("#estado").change(function (e) {
            $("#municipio").empty();
            var options =
                $.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/" + $("#estado option:selected").val() + "/municipios/", function (data) {
                    data.forEach(municipio => {
                        $('#municipio').append(
                            `<option value=\'"${municipio.nome}"\'>
                                ${municipio.nome}
                            </option>`
                        );
                    });
                });
        });
    });
});
