<?php
function abaixo_assinado_cadastrar()
{
?>
    <link rel="stylesheet" href="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastrar\style.css">
    <script type="text/javascript" src="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastrar\script.js"></script>
    <table class="wp-list-table widefat striped">
        <thead>
            <tr>
                <th>Abaixo-assinado</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>
                    <form method="post">
                        <div style="display: flex; flex-direction: column;">
							<object data="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastrar\assets\images\peticao-publica.pdf" type="application/pdf" width="100%" height="565px">
								<embed src="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastrar\assets\images\peticao-publica.jpg" width="100%" height="565px"/>
							</object>
                            <br>
                            <label>Nome&nbsp<a style="color: red;">*</a></label>
                            <input type="text" oninput="validar_nome()" required name="abaixo-assinado-nome" id="nome" placeholder="Nome completo"/>
                            <label>ANAC&nbsp<a style="color: red;">*</a></label>
                            <input type="text" required name="abaixo-assinado-anac" placeholder="Código ANAC " />
                            <label>Estado (opcional)</label>
                            <select id="estado" name="abaixo-assinado-estado">
                                <option value="">Selecione seu Estado</option>
                            </select>
                            <label>Município (opcional)</label>
                            <select id="municipio" name="abaixo-assinado-municipio">
                                <option value="">Selecione seu Município</option>
                            </select>
                            <table class="wp-list-table widefat striped projeto">
                                <thead>
                                    <tr>
                                        <th class="table-header">
                                            <a>Termo de concordância</a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="projeto">
                                    <tr>
                                        <th>
                                            <h6>
                                                Duis eu vehicula nibh. Fusce imperdiet, ante quis maximus interdum, tortor urna aliquet massa, nec lacinia tortor augue nec urna. Phasellus consequat vitae ex vitae consequat. Etiam vel nunc enim. Maecenas eu interdum dolor. Proin at euismod augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id blandit libero, sit amet vehicula leo. Quisque tincidunt magna ligula, eu suscipit diam pellentesque a.
                                            </h6>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="aceite">
                                <input type="checkbox" required id="aceite" name="abaixo-assinado-aceite">
                                <label for="aceite">&nbsp ACEITO</label>
                            </div>
                            <a style="margin-top: 10px; margin-bottom: 5px;" href="">Política de privacidade</a>
                            <input type="submit" required class="button action" name="abaixo-assinado-cadastrar" value="Assinar" />
                        </div>
                    </form>
                </th>
            </tr>
        </tbody>
    </table>
<?php
}
?>
