<?php
require_once(ABAIXO_ASSINADO_LOCAL . 'funcionalidades/cadastrar/html.php');
require_once(ABAIXO_ASSINADO_LOCAL . 'funcionalidades/cadastrar/script.php');
require_once(ABAIXO_ASSINADO_LOCAL . 'funcionalidades/cadastrar/shortcode.php');
add_shortcode('abaixo_assinado_cadastrar', 'abaixo_assinado_cadastrar_shortcode');
add_action('admin_menu', 'abaixo_assinado_cadastrar_formulario');
add_action('template_redirect', 'abaixo_assinado_cadastrar_formulario');
?>