<?php
function gerar_pdf()
{
    if (isset($_POST['pdf'])) {
        global $wpdb;
        $sql = "SELECT * FROM sna_abaixo_assinado";
        $usuarios = $wpdb->get_results($sql);
        if (!is_null($usuarios)) {
            require(plugin_dir_path(__FILE__) . 'assets/fpdf.php');

            class PDF extends FPDF
            {
                function Header()
                {
                    $this->Image(plugin_dir_path(__FILE__) . 'assets/images/marca.jpg', 0, 0, 210, 310);
                }
            }

            $pdf = new PDF();
            $pdf->AddPage();

            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetTextColor(0, 0, 0);
            $txt = "DECLARAÇÃO SOBRE...";
            $pdf->SetXY((210 / 2) - (120 / 2), 30);
            $pdf->MultiCell(120, 5, utf8_decode($txt), 0, 'C');

            $pdf->SetFont('Arial', '', 12);
            //$pdf->SetLeftMargin(25);
            $header = array('Nome', 'Estado', utf8_decode('Município'), 'ANAC');
            $w = array(100, 20, 50, 20);
            for ($i = 0; $i < count($header); $i++)
                $pdf->Cell($w[$i], 7, $header[$i], 1, 0);
            $pdf->Ln();
            foreach ($usuarios as $usuario) {
                $pdf->Cell($w[0], 6, utf8_decode($usuario->nome), 'LR');
                $pdf->Cell($w[1], 6, $usuario->estado, 'LR');
                $pdf->Cell($w[2], 6, utf8_decode($usuario->municipio), 'LR', 0);
                $pdf->Cell($w[3], 6, $usuario->anac, 'LR', 0);
                $pdf->Ln();
            }
            $pdf->Cell(array_sum($w), 0, '', 'T');
            $pdf->AddPage();

            $txt = "São Paulo, Marcelo Rodrigues Campos.";
            $pdf->SetXY($pdf->GetX(), $pdf->GetY() + 200);
            $pdf->MultiCell(0, 5, utf8_decode($txt), 0, 'C');

            $pdf->SetXY(10, $pdf->GetY() + 5);
            $txt = '	
            Atenciosamente,           
            
            
            Marcelo Rodrigues Campos
            Desenvolvedor Fullstack e Mobile';
            $pdf->MultiCell(0, 6, utf8_decode($txt), 0, 'J');
            $pdf->Image(plugin_dir_path(__FILE__) . 'assets/images/assinatura.png', $pdf->GetX() + 4, $pdf->GetY() - 23, 85, 10);
            $pdf->Output();

            exit();
        } else {
            print_r("Sentimos muito mas esta carta não está em nossas listas. Favor entrar em contato com o SNA.");
            die();
        }
    }
}