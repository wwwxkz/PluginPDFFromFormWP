var script = document.createElement('script');
script.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
document.getElementsByTagName('head')[0].appendChild(script);

function fechar_popup() {
    modal.style.display = "none";
}

window.onclick = function (event) {
    if (typeof modal !== 'undefined') {
        if (event.target == modal) {  
            fechar_popup();
        }
    }
}

var jquery = function (callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function () { jquery(callback); }, 20);
    }
};

function mostrar_popup(id) {
    modal = document.getElementById(id);
    if (modal.style.display == "none") {
        if(id.includes("-edit")){
            mostrar_ibge(id);
        }
        modal.style.display = "block";
    } else {
        modal.style.display = "none";
    }
}

function mostrar_ibge(id) {
    estado_select = id.replace('-edit','-estado');
    municipio_select = id.replace('-edit','-municipio');
    jquery(function ($) {
        $(function () {
            $.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/", function (data) {
                data.forEach(estado => {
                    $('#' + estado_select).append(
                        `<option value="${estado.sigla}">
                            ${estado.nome}
                        </option>`
                    );
                });
            });
    
            $("#"  + estado_select).change(function (e) {
                $("#" + municipio_select).empty();
                var options =
                    $.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/" + $("#" + estado_select + " option:selected").val() + "/municipios/", function (data) {
                        data.forEach(municipio => {
                            $('#' + municipio_select).append(
                                `<option value=\'"${municipio.nome}"\'>
                                    ${municipio.nome}
                                </option>`
                            );
                        });
                    });
            });
        });
    });
}
