<?php
function abaixo_assinado_cadastros()
{
    global $wpdb;
    $sql = "SELECT * FROM sna_abaixo_assinado";
    $usuarios = $wpdb->get_results($sql);
    ?>
    <link rel="stylesheet" href="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastros\style.css">
    <script type="text/javascript" src="..\wp-content\plugins\sna_abaixo-assinado\funcionalidades\cadastros\script.js"></script>
    <div style="float: left; margin-top: 15px; padding: 0;">
        <h2>Listagem Abaixo-assinado</h2>
        <form method="post">
            <input type="submit" class="button action" name="pdf" value="Gerar PDF" />
        </form>
        <br>
        <table class="wp-list-table widefat fixed striped table-view-list">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Criado</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Município</th>
                    <th scope="col">ANAC</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($usuarios as $usuario) {
                ?>
                    <tr>
                        <th scope="row"><?php echo $usuario->id; ?></th>
                        <th><?php echo $usuario->criado; ?></th>
                        <th><?php echo $usuario->nome; ?></th>
                        <th><?php echo $usuario->estado; ?></th>
                        <th><?php echo $usuario->municipio; ?></th>
                        <th><?php echo $usuario->anac; ?></th>
                        <th>
                            <form method="post">
                                <button type="button" class="button action" onclick="mostrar_popup('<?php echo $usuario->id; ?>-edit');">Editar</button>
                                <input type="submit" class="button action" name="<?php echo $usuario->id; ?>-deletar" value="Deletar" />
                                <div id="<?php echo $usuario->id; ?>-edit" class="modal" style="display: none;">
                                    <input type="hidden" name="id" value="<?php echo $usuario->id; ?>">
                                    <div class="conteudo-modal" style="display: flex; flex-direction: column;">
                                        <span onclick="fechar_popup()" class="fechar">&times;</span>
                                        <label>Nome</label>
                                        <input type="text" name="nome" value="<?php echo $usuario->nome; ?>" placeholder="Nome completo" />
                                        <label>Estado</label>
                                        <select id="<?php echo $usuario->id; ?>-estado" name="estado">
                                            <option value="">Selecione seu Estado</option>
                                        </select>
                                        <label>Município</label>
                                        <select id="<?php echo $usuario->id; ?>-municipio" name="municipio">
                                            <option value="">Selecione seu Município</option>
                                        </select>
                                        <label>ANAC</label>
                                        <input type="text" name="anac" value="<?php echo $usuario->anac; ?>" placeholder="Código ANAC" />
                                        <br>
                                        <input type="submit" class="button action" name="editar" value="Editar" />
                                    </div>
                                </div>
                            </form>
                        </th>
                    </tr>
                <?php
                    if (isset($_POST[$usuario->id . '-deletar'])) {
                        $wpdb->get_results("DELETE FROM sna_abaixo_assinado WHERE id=$usuario->id");
                        echo "<script>window.location.reload()</script>";
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
<?php
}
?>
